#!/bin/bash
qemu-system-aarch64 -M virt -m 512 -cpu cortex-a53 -smp 2 \
  -kernel vmlinuz-5.10.0-9-arm64 \
  -initrd initrd.img-5.10.0-9-arm64 \
  -append 'root=/dev/vda2' \
  -drive if=none,file=hda.qcow2,format=qcow2,id=hd \
  -device virtio-blk-pci,drive=hd \
  -netdev user,id=mynet,hostfwd=tcp::22-:22\
  -device virtio-net-pci,netdev=mynet \
  -no-reboot \
  -nographic \
  -device qemu-xhci -device usb-kbd -device usb-tablet

#-display gtk,gl=on \
#-device virtio-gpu \

