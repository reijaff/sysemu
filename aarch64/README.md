## Debian GNU/Linux 11 (bullseye)
### start
`$ docker run --rm -it registry.gitlab.com/reijaff/sysemu:aarch64`

ssh default config, port 22 
creds:
- `user:user`

root creds: `root:toor`

based on guides :
- https://gamedev.ru/flame/forum/?id=263879&m=5456066#m3
- https://www.debian.org/releases/stretch/arm64/
- https://translatedcode.wordpress.com/2017/07/24/installing-debian-on-qemus-64-bit-arm-virt-board/



### misc

```
$ uname -a
Linux debian 5.10.0-9-arm64 #1 SMP Debian 5.10.70-1 (2021-09-30) aarch64 GNU/Linux
```


starts aprox. 70 seconds




